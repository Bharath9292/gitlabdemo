package stepdefinations;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Login {

	public WebDriver driver;
	
	
	@SuppressWarnings("deprecation")
	@Given("user is on login page")
	public void user_is_on_login_page() {
	    
		 WebDriverManager.chromedriver().setup();
		 driver=new ChromeDriver();
		 Timeouts time=driver.manage().timeouts();
		 time.pageLoadTimeout(25,TimeUnit.SECONDS);
		 
		driver.get("https://practicetestautomation.com/practice-test-login/");
	}
	@When("user login to application with username and password")
	public void user_login_to_application_with_username_and_password() {
	   
		driver.findElement(By.id("username")).sendKeys("student");
		driver.findElement(By.id("password")).sendKeys("Password123");

	}
	@When("click on login")
	public void click_on_login() {
		driver.findElement(By.id("submit")).click();
	}
	@Then("user lands on the home page")
	public void user_lands_on_the_home_page() {
	    
		driver.getTitle();
		driver.quit();
	}
	
	
	
	
}
