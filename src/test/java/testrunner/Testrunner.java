package testrunner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(
		         features="src/test/java/features", glue="stepdefinations"
		                 )

public class Testrunner extends AbstractTestNGCucumberTests {

}
